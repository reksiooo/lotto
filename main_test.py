from main import draw_numbers


def test_draw_numbers():
    drawn_numbers = sorted(list(draw_numbers()))

    for _ in range(501):
        assert len(drawn_numbers) == 6
        assert drawn_numbers[0] >= 1
        assert drawn_numbers[-1] <= 49