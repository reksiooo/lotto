""" Application for Totolotek """
import random


WINNING_PRICE = 5000000
WINNING_PRICE_FOR_3 = 25
WINNING_PRICE_FOR_4 = 200
WINNING_PRICE_FOR_5 = 6000

COST_OF_SINGLE_DRAW = 3
my_numbers = {3, 6, 8, 19, 22, 37}
all_possible_numbers = range(1,50)


def draw_numbers() -> set:
    """Draw six lotto numbers

    Returns:
        set: set with 6 different numbers from range 1 to 49
    """
    return set(random.sample(all_possible_numbers, k=6))


def play_until_you_win(numbers, drawing_numbers_algorithm):
    """Keep drawing numbers until you win

    Args:
        numbers (set): user choice numbers

    Returns:
        int: number of attemps to win
    """
    random_numbers = set()
    #counter = 0
    counters = {'counter': 0, 3: 0, 4: 0, 5: 0}

    while numbers != random_numbers:
        random_numbers = drawing_numbers_algorithm()
        common_numbers = numbers.intersection(random_numbers)
        common_numbers_len = len(common_numbers)
        counters['counter'] += 1
        if 3 <= common_numbers_len <= 5:
            counters[common_numbers_len] += 1
    return counters

if __name__ == '__main__':
    my_counters  = play_until_you_win(my_numbers, draw_numbers)
    COUNTER = my_counters['counter']
    TOTAL_COST = COST_OF_SINGLE_DRAW * COUNTER

    player_age = int(input('Ile masz lat?'))

    TOTAL_WINNING = (
        WINNING_PRICE +
        WINNING_PRICE_FOR_3 * my_counters[3] +
        WINNING_PRICE_FOR_4 * my_counters[4] +
        WINNING_PRICE_FOR_5 + my_counters[5]
    )

    print(f'Koszt inwetycji wynosi: {TOTAL_COST:,}')
    print(f'Wygrana suma wynosi: {TOTAL_WINNING:,}')
    print(f'Twój bilans wynosi: {WINNING_PRICE - TOTAL_COST:,}')
    print(f'Wygranie szóstki zajęło Ci {COUNTER} tygodni, '
          f'{int(COUNTER/4)} miesięcy, {int(COUNTER/48)} lat')
    print(f'Szótkę trafisz w wieku {player_age + int(COUNTER/48)} lat')
    print(f'W tym czasie trafiłeś również {my_counters[3]} razy trójkę, '
          f'{my_counters[4]} razy czwórkę, {my_counters[5]} razy piątkę')
